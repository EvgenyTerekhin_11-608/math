﻿using System;

namespace TwoConveyers
{
    class Program
    {
        static int LinesCount = 2;
        static int WorkPlacesCount = 4;
        static int[,] Journal = new int[2, WorkPlacesCount];

        static int GetBestPath(int[,] a, int[,] t, int[] e, int[] x)
        {
            var TFirstConc = new int [WorkPlacesCount];
            var TSecondConv = new int[WorkPlacesCount];
            int i;

            TFirstConc[0] = e[0] + a[0, 0];

            TSecondConv[0] = e[1] + a[1, 0];

            for (i = 1; i < WorkPlacesCount; ++i)
            {
                if (TFirstConc[i - 1] + a[0, i] <= TSecondConv[i - 1] + t[1, i - 1] + a[0, i])
                {
                    Journal[0, i - 1] = 0;
                    TFirstConc[i] = TFirstConc[i - 1] + a[0, i];
                }
                else
                {
                    Journal[0, i - 1] = 1;
                    TFirstConc[i] = TSecondConv[i - 1] + t[1, i - 1] + a[0, i];
                }

                if (TSecondConv[i - 1] + a[1, i] <= TFirstConc[i - 1] + t[0, i - 1] + a[1, i])
                {
                    Journal[0, i - 1] = 1;
                    TSecondConv[i] = TSecondConv[i - 1] + a[1, i];
                }
                else
                {
                    Journal[0, i - 1] = 0;
                    TSecondConv[i] = TFirstConc[i - 1] + t[0, i - 1] + a[1, i];
                }
            }


            if (TFirstConc[WorkPlacesCount - 1] + x[0] <= TSecondConv[WorkPlacesCount - 1] + x[1])
            {
                Journal[0, WorkPlacesCount - 1] = 0;
                Journal[1, WorkPlacesCount - 1] = 0;
                return TFirstConc[WorkPlacesCount - 1] + x[0];
            }

            Journal[0, WorkPlacesCount - 1] = 1;
            Journal[1, WorkPlacesCount - 1] = 1;
            return TSecondConv[WorkPlacesCount - 1] + x[1];
        }

        static void Print()
        {
            var conveyor = Journal[0, WorkPlacesCount - 1];
            Console.WriteLine($"Conveyor {conveyor + 1}, place {WorkPlacesCount}");

            for (var i = WorkPlacesCount - 1; i >= 1; i--)
            {
                conveyor = Journal[conveyor, i];
                Console.WriteLine($"Conveyor {conveyor + 1}, place {i + 1}");
            }
        }


        static void Main(string[] args)
        {
            // стоимости работ на каждом конвейере
            int[,] a =
            {
                {10, 100, 10, 100, 13},
                {12, 120, 131, 24, 13}
            };

            // время переходов с одного конвейера на другой
            int[,] t =
            {
                {11, 20, 30, 41},
                {30, 5, 24, 53}
            };
            // время шага на первый и второго контвейера
            var e = new[] {11, 12};
            // время схода с первого и второго конвейера
            var x = new[] {200, 100};

            Console.WriteLine(GetBestPath(a, t, e, x));
            Print();
        }
    }
}