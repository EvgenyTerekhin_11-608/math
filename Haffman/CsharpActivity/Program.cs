﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CsharpActivity
{
    //haffman
    public class Node
    {
        public string Title { get; }
        public int Frequency { get; }

        public Node Left { get; }

        public Node Right { get; }

        public Node(Tree left, Tree right)
        {
            Frequency = left.Root.Frequency + right.Root.Frequency;
            Left = left.Root;
            Right = right.Root;
        }

        public Node(int frequency, string title)
        {
            Frequency = frequency;
            Title = title;
        }
    }

    public class Tree : IComparable
    {
        public Node Root { get; }

        public Tree(Node root)
        {
            Root = root;
        }


        public int CompareTo(object obj)
        {
            var tree = (Tree) obj;
            return tree.Root.Frequency < Root.Frequency ? 1 : 0;
        }
    }


    class Program
    {
        static async Task<int> Main()
        {
            var queue = new PriorityQueue<Tree>();
            var words = (await File.ReadAllLinesAsync(Directory.GetCurrentDirectory() + "/input.txt")).ToList();
            words.ForEach(x =>
            {
                var spl = x.Split(' ');
                var node = new Tree(new Node(int.Parse(spl[1]), spl[0]));
                queue.Enqueue(node);
            });

            while (queue.Count > 1)
            {
                var a = queue.Dequeue();
                var b = queue.Dequeue();
                queue.Enqueue(new Tree(new Node(a, b)));
            }

            var dictionary = new Dictionary<string, string>();
            GetTable(queue.Dequeue().Root, string.Empty, dictionary);

            foreach (var keyvalue in dictionary)
            {
                Console.WriteLine($"{keyvalue.Key} {keyvalue.Value}");
            }

            return 1;
        }

        private static void GetTable(Node tree, string seq, IDictionary<string, string> dictionary)
        {
            if (tree.Title == null)
            {
                if (tree.Left.Frequency > tree.Right.Frequency)
                {
                    GetTable(tree.Left, seq + "1", dictionary);
                    GetTable(tree.Right, seq + "0", dictionary);
                }
                else
                {
                    GetTable(tree.Left, seq + "0", dictionary);
                    GetTable(tree.Right, seq + "1", dictionary);
                }
            }
            else dictionary.Add(tree.Title, seq);
        }
    }
}