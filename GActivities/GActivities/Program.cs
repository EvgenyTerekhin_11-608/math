﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GActivities
{
    public class Activity
    {
        public int StartHour { get; set; }
        public int EndHour { get; set; }
    }

    class Program
    {

        static void Main(string[] args)
        {
            var activities = File.ReadAllLines(Directory.GetCurrentDirectory() + "/input.txt")
                .Skip(1)
                .Select(x =>
                {
                    var spl = x.Split(' ').Select(int.Parse).ToList();
                    return new Activity {EndHour = spl[1], StartHour = spl[0]};
                })
                .ToArray();

            var i = 0;
            Console.WriteLine(i);
            for (var ii = 1; ii < activities.Length; ii++)
            {
                if (activities[ii].StartHour < activities[i].EndHour) continue;
                Console.WriteLine($"{ii} ");
                i = ii;
            }
        }
    }
}