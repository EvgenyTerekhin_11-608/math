﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Gragham
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }


    class Program
    {
        static double Rotate(Point a, Point b, Point c)
        {
            return (b.X - a.X) * (c.Y - b.Y) - (b.Y - a.Y) * (c.X - b.X);
        }

        static int getSecondItemStack(Stack<int> stack)
        {
            var item = stack.Pop();
            var r = stack.Peek();
            stack.Push(item);
            return r;
        }

        static List<int> SortOneElement(Point[] points)
        {
            var arr = Enumerable.Range(0, points.Length).ToList();

            for (var i = 0; i < arr.Count; i++)
            {
                if (points[arr[i]].X > points[arr[0]].X) continue;
                var ii = arr[i];
                arr[i] = arr[0];
                arr[0] = ii;
            }

            return arr;
        }

        static Point[] Grapcham(List<Point> points)
        {
            var arr = SortOneElement(points.ToArray());
            for (var i = 2; i < arr.Count; i++)
            {
                var j = i;
                while (j > 1 && Rotate(points[arr[0]], points[arr[j - 1]], points[arr[j]]) < 0)
                {
                    var ii = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = ii;
                    j--;
                }
            }

            var stack = new Stack<int>();
            stack.Push(arr[0]);
            stack.Push(arr[1]);
            for (var i = 2; i < arr.Count; i++)
            {
                while (Rotate(points[getSecondItemStack(stack)], points[stack.Peek()], points[arr[i]]) < 0) stack.Pop();
                stack.Push(arr[i]);
            }

            return stack.Select(x => points[x]).ToArray();
        }

        static Point[] Jarvis(List<Point> points)
        {
            var arr = SortOneElement(points.ToArray());
            var stack = new List<int> {arr[0]};
            arr.Add(arr[0]);
            arr.RemoveAt(0);
            while (true)
            {
                var rigth_point = 0;
                for (int i = 1; i < arr.Count; i++)
                {
                    if (Rotate(points[stack[stack.Count - 1]], points[arr[rigth_point]], points[arr[i]]) < 0)
                        rigth_point = i;
                }

                if (arr[rigth_point] == stack.First())
                    break;
                stack.Add(arr[rigth_point]);
                arr.RemoveAt(rigth_point);
            }

            return stack.Select(x => points[x]).ToArray();
        }

        static async Task<int> Main(string[] args)
        {
            var path = $"{Directory.GetCurrentDirectory()}/input.txt";
            var points = (await File.ReadAllLinesAsync(path))
                .Select(x =>
                {
                    var spl = x.Split(' ').Select(int.Parse).ToList();
                    return new Point {X = spl[0], Y = spl[1]};
                }).ToList();


            var list = Grapcham(points);
            Print("Grapcham", list);
            list = Jarvis(points);
            Print("Jarvis", list);

            return 0;
        }

        static void Print(string methodname, Point[] points)
        {
            Console.WriteLine(methodname);
            foreach (var l in points)
            {
                Console.WriteLine($"X {l.X} Y {l.Y}");
            }

            Console.WriteLine();
        }
    }
}